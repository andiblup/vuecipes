// Composables
import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import NewRecipeView from '@/views/NewRecipeView.vue'
import ReworkRecipeView from '@/views/ReworkRecipeView.vue'
import ReworkIngredientView from '@/views/ReworkIngredientView.vue'
import ChangeStepView from '@/views/ChangeStepView.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },{
      path: '/newRecipe',
      name: 'newRecipe',
      component: NewRecipeView
    },{
      path: '/maintain/:recipeId',
      name: 'maintain/:recipeId',
      component: ReworkRecipeView
    },{
      path: '/maintain/:recipeId/ingredients/:ingredientId',
      name: 'maintain/:recipeId/ingredients/:ingredientId',
      component: ReworkIngredientView
    },{
      path: '/maintain/:recipeId/steps/:stepId',
      name: '/maintain/:recipeId/steps/:stepId',
      component: ChangeStepView
    }
  ]
})

export default router
