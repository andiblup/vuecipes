import { defineStore } from "pinia";
import axios from "axios";
import { ref, watch, computed } from "vue";
import { useRecipesStore } from "@/store/recipes";
import { useIngredientsStore } from "@/store/ingredients";

export const useRedundancyStore = defineStore("redundancy", () => {
  const recipesStore = useRecipesStore();
  const ingredientsStore = useIngredientsStore();

  const allRecipesInfo = ref([]);

  async function prepareRecipes() {
    const infos = ref([]);

    recipesStore.allRecipes.forEach((recipe) => {
      const obj = ref({});
      const searchedIngredients = ref([]);

      // for each ingredient
      recipe.ingredients.forEach(async (neededIngredient) => {
        // get ingr data
        let ingr = await ingredientsStore.getSpecificIngredient(
          neededIngredient.ingredientId
        );
        // alle daten einer ingredient werden zwischengespeichert
        searchedIngredients.value.push({
          ingredientId: ingr.ingredientId,
          name: ingr.name,
          unit: ingr.unit,
          amount: neededIngredient.amount,
        });
      });
      obj.value = {
        recipeId: recipe.recipeId,
        createdAt: recipe.createdAt,
        updatedAt: recipe.updatedAt,
        name: recipe.name,
        introduction: recipe.introduction,
        steps: recipe.steps,
        ingredients: searchedIngredients.value,
      };
      infos.value.push(obj);
    });
    /*sinfos.value.forEach((element) => {
      //console.log("ANANAS");
      //console.log(element);
    });*/

    allRecipesInfo.value = infos.value;
  }

  async function refreshDataLoad(){
    await recipesStore.axiosSyncRecipes();
    await ingredientsStore.axiosSyncAllIngredients();
    this.prepareRecipes();
  }



  return { allRecipesInfo, prepareRecipes, refreshDataLoad};
});
