// Utilities
import { defineStore } from "pinia";
import axios from "axios";

export const useIngredientsStore = defineStore('ingredients', () => {

  const URL = 'https://kprsrvghyo.recipes.asw.rest/api/'


  let allIngredients = [];







  function addIngredient(ingredient){
    allIngredients.push(ingredient);
  }



  //! AXIOS
  // POST with auto sync
  async function axiosCreateIngredient(obj){
    const response = await axios.post(URL + "ingredients", {
      name: obj.name,
      unit: obj.unit
    });
    allIngredients.push(response.data);
    /*allIngredients.push(response.data);
    return allIngredients[allIngredients.length - 1];*/
    return response.data;
  }

  // GET ALL
  async function axiosGetAllIngredient(){
    const response = await axios.get(URL + "ingredients");
    return response.data;
  }

  // Sync with backend
  async function axiosSyncAllIngredients(){
    const response = await axios.get(URL + "ingredients");
    allIngredients = response.data;
  }

  // GET spec
  async function getSpecificIngredient(id){
    const response = await axios.get(URL + "ingredients/" + id);
    return response.data;
  }

  // PUT spec
  async function changeSpecificIngredient(id, obj){
    const response = await axios.put(URL + "ingredients/" + id,{
      name: obj.name,
      unit: obj.unit
    });
    return response.data;
  }

  // DELETE spec
  async function deleteIngredient(id){
    const response = await axios.delete(URL + "ingredients/" + id)
  }



  return {
    allIngredients, addIngredient, axiosCreateIngredient,
    axiosGetAllIngredient, axiosSyncAllIngredients, getSpecificIngredient,
    changeSpecificIngredient, deleteIngredient
  }
})
