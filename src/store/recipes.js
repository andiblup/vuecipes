import { defineStore } from "pinia";
import axios from "axios";
import { ref } from "vue";

export const useRecipesStore = defineStore("recipes", () => {
  // https://kprsrvghyo.recipes.asw.rest/docs
  const URL = "https://kprsrvghyo.recipes.asw.rest/api/";

  const allRecipes = ref([]);

  /*
  function addRecipe(recipe){
    allRecipes.value.push(recipe);
  }
*/

  //! Axios
  // POST RECIPE
  async function axiosCreateNewRecipe(recipe) {
    const response = await axios.post(URL + "recipes/", {
      name: recipe.name,
      introduction: recipe.introduction,
      steps: recipe.steps,
      ingredients: recipe.ingredients,
    });
  }

  // Sync
  async function axiosSyncRecipes() {
    const response = await axios.get(URL + "recipes/");
    allRecipes.value = response.data;
  }

  // Delete specific recipe
  async function axiosDeleteSpecificRecipe(id) {
    const response = await axios.delete(URL + "recipes/" + id);
    return response.data;
  }

  // Delete ingredient from recipe
  async function deleteIngredientFromRecipe(recipeId, ingredientId){
    const response = await axios.delete(URL + "recipes/" + recipeId + "/ingredients/" + ingredientId);
  }

  // POST ingredient to recipe
  async function addExistingIngredientToRecipe(recipeId, ingredientId, obj){
    //console.log(recipeId, ingredientId, obj);
    const response = await axios.post(URL + "recipes/" + recipeId + "/ingredients/" + ingredientId, {
      "amount": parseInt(obj.amount)
    });
    return response.data;
  }

  // PUT change existing ingredient in recipe / if ingredient exists(only changes at amount)
  async function changeIngredientInRecipe(recipeId, ingredientId, obj){
    const response = await axios.post(URL + 'recipes/' + recipeId + '/ingredients/' + ingredientId, {
      amount: obj.amount
    });
    return response.data;
  }


  // PUT change an existing recipe
  async function changeRecipe(recipeId, obj){
    const response = await axios.put(URL + "recipes/" + recipeId, {
      name: obj.name,
      introduction: obj.introduction,
      steps: obj.steps,
      ingredients: obj.ingredients
    });
    return response.data;
  }

  // GET specific recipe
  async function getSpecificRecipe(recipeId){
    const response = await axios.get(URL + "recipes/" + recipeId);
    return response.data;
  }




  //! STEPS
  async function changeExistingStep(stepId, obj){
    const response = await axios.put(URL + "steps/" + stepId, {
      duration: obj.duration,
      description: obj.description
    });
    return response.data;
  }

  async function deleteStep(stepId){
    const response = await axios.delete(URL + "steps/" + stepId);
    return response.data;
  }


  return {
    allRecipes, axiosCreateNewRecipe,
    axiosSyncRecipes, axiosDeleteSpecificRecipe,
    deleteIngredientFromRecipe, addExistingIngredientToRecipe,
    changeRecipe, getSpecificRecipe, changeIngredientInRecipe,
    changeExistingStep, deleteStep
  };
});
